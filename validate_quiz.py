import random
from difflib import SequenceMatcher

def validate(question, answers):
	lines = []
	string = ""
	boolean = True

	f = open('quiz_log.txt')
	lines = f.readlines()
	f.close()

	for i in range(len(lines)):
		lines[i] = lines[i].replace('\n','')
		lines[i] = lines[i].split(' | ')

	for i in range(len(lines)):
		if question == lines[i][0]:
			if lines[i][1] == '?':
				break

			index = -1
			max = -1
			for j in range(len(answers)):
				if answers[j] == 'Shark' or answers[j] == 'Rat':
					return chr(65+j)
				elif SequenceMatcher(None, lines[i][1], answers[j]).ratio() > max:
					index = j
					max = SequenceMatcher(None, lines[i][1], answers[j]).ratio()
			return chr(65+index)
			# return lines[i][1]
	else:
		lines.append([question, '?'])
		for i in range(len(lines)):
			string += lines[i][0] + ' | ' + lines[i][1]
			if i != len(lines) - 1:
				string += '\n'

		f = open('quiz_log.txt','w')
		f.write(string)
		f.close()

	return chr(random.randint(65,64+len(answers)))