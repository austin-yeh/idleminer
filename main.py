import requests
import asyncio, time
import json
import random
import os, sys, signal, re
from dotenv import load_dotenv
import validate_quiz


load_dotenv()

open('last_session_log.txt', 'w').close()

def send(link, content):
	r = requests.post(link, data={"content": content}, headers={"authorization":os.getenv('token')})

def retrieve(link, index=0):
	r = requests.get(link, headers={"authorization":os.getenv('token')})
	jsonn = json.loads(r.text)
	return jsonn[index]


async def check_if_warned():
	while True:
		msg = retrieve(os.getenv('yeh_idle_dm'))
		if (not msg.get('content') == '**Thank you for voting**\nYou received a Vote crate') and (msg.get('author').get('username')=='Idle Miner'):
			r = requests.post(os.getenv('ayana'), data={"content": '<@!596362255101198336>'}, headers={"authorization":os.getenv('yeh_token')})
			print('Inormality Detected', time.strftime("%H:%M:%S", time.localtime()))
			with open('last_session_log.txt', 'a')as f:
				f.write(str('Inormality Detected'+' '+ time.strftime("%H:%M:%S", time.localtime()) + '\n'))
			exit()
		else:
			await asyncio.sleep(1)

async def claim_and_open():
	while True:
		send(os.getenv('sup'), ';sup')
		await asyncio.sleep(1)
		send(os.getenv('claim-and-open'), ';ca')
		currtime = '| ' +time.strftime("%H:%M:%S", time.localtime())
		print('Gift: claimed', currtime)
		with open('last_session_log.txt', 'a')as f:
			f.write(str('Gift: claimed'+' '+ currtime + '\n'))
		await asyncio.sleep(1)
		send(os.getenv('claim-and-open'), ';o a')
		currtime = '| ' +time.strftime("%H:%M:%S", time.localtime())
		print('Gift: opened', currtime)
		with open('last_session_log.txt', 'a') as f:
			f.write(str('Gift: opened'+' '+ currtime + '\n'))
		await asyncio.sleep(3600)

async def hunt_and_fish():
	while True:
		# Hunt
		send(os.getenv('hunt-and-fish'), ';h')
		currtime = '| ' + time.strftime("%H:%M:%S", time.localtime())
		await asyncio.sleep(1)
		content = (retrieve(os.getenv('hunt-and-fish'))).get('content')
		if "Please wait" in content:
			print('Hunt: needs', content.split(' ')[3], currtime)
			with open('last_session_log.txt', 'a') as f:
				f.write(str('Hunt: needs'+' '+ content.split(' ')[3]+' '+ currtime + '\n'))
		elif "You didn't get any pet" in content:
			print('Hunt: failed', currtime)
			with open('last_session_log.txt', 'a') as f:
				f.write(str('Hunt: failed'+' '+ currtime + '\n'))
		else:
			print('Hunt: succeeded', currtime)
			with open('last_session_log.txt', 'a') as f:
				f.write(str('Hunt: succeeded' + ' '+ currtime + '\n'))

		await asyncio.sleep(1)

		# Fish
		send(os.getenv('hunt-and-fish'), ';f')
		currtime = '| ' +time.strftime("%H:%M:%S", time.localtime())
		await asyncio.sleep(1)
		msg = retrieve(os.getenv('hunt-and-fish'))
		try:
			if 'can fish again in' in msg.get('embeds')[0].get('description'):
				print("Fish: can't fish yet", currtime)
				with open('last_session_log.txt', 'a') as f:
					f.write(str("Fish: can't fish yet"+' '+ currtime + '\n'))
			else:
				print("Fish: succeeded", currtime)
				with open('last_session_log.txt', 'a') as f:
					f.write(str("Fish: succeeded"+' '+ currtime + '\n'))
		except:
			print('Fish: succeeded', currtime)
			with open('last_session_log.txt', 'a') as f:
				f.write(str('Fish: succeeded' + ' '+ currtime + '\n'))

		await asyncio.sleep(1)

		# Quiz
		send(os.getenv('quiz'), ';q')
		currtime = '| ' +time.strftime("%H:%M:%S", time.localtime())
		await asyncio.sleep(5)
		embeds = retrieve(os.getenv('quiz')).get('embeds')
		if embeds != []: 
			question = embeds[0].get('fields')[0].get('name').replace('*','')
			answers = embeds[0].get('fields')[0].get('value').split('\n')
			for i in range(len(answers)):
				answers[i] = answers[i][answers[i].find(' ')+1:]

			send(os.getenv('quiz'), validate_quiz.validate(question, answers))
			await asyncio.sleep(5)
			content = retrieve(os.getenv('quiz')).get('content')
			if 'answer is correct' in content:
				print('Quiz: answered correctly.', currtime)
				with open('last_session_log.txt', 'a') as f:
					f.write(str('Quiz: answered correctly.' + ' '+ currtime + '\n'))
			else:
				print('Quiz: answered incorrectly.', currtime)
				with open('last_session_log.txt', 'a') as f:
					f.write(str('Quiz: answered incorrectly.' + ' '+ currtime + '\n'))

		else:
			print('Quiz: not available yet.', currtime)
			with open('last_session_log.txt', 'a') as f:
				f.write(str('Quiz: not available yet.' + ' '+ currtime + '\n'))

		await asyncio.sleep(random.gauss(310,4))
		if random.randint(1,6)==1:
			await asyncio.sleep(random.gauss(310,4))

async def pets():
	while True:
		send(os.getenv('pets'), ';p')
		await asyncio.sleep(1)
		items = retrieve(os.getenv('pets')).get('embeds')[0].get('fields')
		shards = int(re.sub("[^0-9]", "", items[1].get('value').split('\n')[0].split(' ')[1]))
		rebirth = int(re.sub("[^0-9]", "", items[1].get('value').split('\n')[1]))
		send(os.getenv('pets'), ';pets')
		await asyncio.sleep(1)
		try:
			common_sum = []
			common_name = []
			uncommon_sum = []
			uncommon_name = []
			rare_sum = []
			rare_name = []
			if '**Common**' in retrieve(os.getenv('pets')).get('embeds')[0].get('fields')[0].get('name'):
				common = retrieve(os.getenv('pets')).get('embeds')[0].get('fields')[0].get('value').split('\n')[1:]
				common = [common[i].split('** ') for i in range(len(common))]
				for pet in common:
					common_sum.append(int(re.sub("[^0-9]", "", pet[1])))
					common_name.append(pet[0].split(' **')[1])

			if '**Uncommon**' in retrieve(os.getenv('pets')).get('embeds')[0].get('fields')[1].get('name'):
				uncommon = retrieve(os.getenv('pets')).get('embeds')[0].get('fields')[1].get('value').split('\n')[1:]
				uncommon = [uncommon[i].split('** ') for i in range(len(uncommon))]
				for pet in uncommon:
					uncommon_sum.append(int(re.sub("[^0-9]", "", pet[1])))
					uncommon_name.append(pet[0].split(' **')[1])

			if '**Rare**' in retrieve(os.getenv('pets')).get('embeds')[0].get('fields')[2].get('name'):
				rare = retrieve(os.getenv('pets')).get('embeds')[0].get('fields')[2].get('value').split('\n')[1:]
				rare = [rare[i].split('** ') for i in range(len(rare))]
				for pet in rare:
					rare_sum.append(int(re.sub("[^0-9]", "", pet[1])))
					rare_name.append(pet[0].split(' **')[1])
			max_level = 5*(rebirth+1)

			if sum(uncommon_sum) < 125:
				new_sum = sum(uncommon_sum)
				for i in range(len(uncommon_sum)):
					if new_sum < min(125, max_level*len(uncommon_sum)) and shards > 20:
						send(os.getenv('pets'), f';up {uncommon_name[i]} {min(int(shards/20), max_level-uncommon_sum[i])}')
						new_sum += min(int(shards/20), max_level-uncommon_sum[i])
						shards -= int(shards/20)*20
						await asyncio.sleep(1)
					else:
						break
			if sum(common_sum) < 25:
				new_sum = sum(common_sum)
				for i in range(len(common_sum)):
					if new_sum < min(25, max_level*len(common_sum)) and shards > 10:
						send(os.getenv('pets'), f';up {common_name[i]} {min(int(shards/10), max_level-common_sum[i])}')
						new_sum += min(int(shards/10), max_level-common_sum[i])
						shards -= int(shards/10)*10
						await asyncio.sleep(1)
					else:
						break
			if sum(rare_sum) < 50:
				new_sum = sum(rare_sum)
				for i in range(len(rare_sum)):
					if new_sum < min(50, max_level*len(rare_sum)) and shards > 50:
						send(os.getenv('pets'), f';up {rare_name[i]} {min(int(shards/50), max_level-rare_sum[i])}')
						new_sum += min(int(shards/50), max_level-rare_sum[i])
						shards -= int(shards/50)*50
						await asyncio.sleep(1)
					else:
						break
			await asyncio.sleep(1200)
			print('upgraded pets without error')
		except Exception as e:
			print('Error occured during pet upgrade')


async def main():
	count = 0
	while True:	
		try:
			send(os.getenv('secret-mine'),';bp')
			await asyncio.sleep(1)
			items = retrieve(os.getenv('secret-mine')).get('embeds')[0].get('fields')[0].get('value').split('\n')
			bp_level = int(re.sub("[^0-9]", "", items[1].replace('*',"")))
			bp_full = items[3].replace('*','').replace('[','').replace(']','').count('x')

			if bp_full >= random.randint(4,6) or count >= random.randint(3, 5):
				count = 0
				send(os.getenv('secret-mine'),';s')
				await asyncio.sleep(1)
				send(os.getenv('secret-mine'),';p')
				await asyncio.sleep(1)
				items = retrieve(os.getenv('secret-mine')).get('embeds')[0].get('fields')[3].get('value').split('\n')[1]
				pick_level = int(re.sub("[^0-9]", "", items))
				items = retrieve(os.getenv('secret-mine')).get('embeds')[0].get('fields')[1].get('value').split('\n')[1]
				rb = int(re.sub("[^0-9]", "", items))
				
				if rb >=25 and pick_level == 200 and bp_level == 200:
					send(os.getenv('secret-mine'),';prestige')
				elif pick_level < bp_level:
					send(os.getenv('secret-mine'),';up p a')
				elif pick_level < 100:
					send(os.getenv('secret-mine'),';up b h')
					await asyncio.sleep(1)
					send(os.getenv('secret-mine'),';up p a')
				elif bp_level < 100:
					send(os.getenv('secret-mine'),';up b a')
				elif pick_level < 200:
					send(os.getenv('secret-mine'),';up p a')
				elif bp_level < 200:
					send(os.getenv('secret-mine'),';up b a')
				else:
					send(os.getenv('secret-mine'),';rb')
				await asyncio.sleep(2)
			else:
				await asyncio.sleep(random.randint(8,10))
				count+=1
		except Exception as e:
			print(e)
			with open('last_session_log.txt', 'a') as f:
				f.write(str('Main error: ' + str(e) + ' | ' +time.strftime("%H:%M:%S", time.localtime()) + '\n'))




loop = asyncio.get_event_loop()
try:
    asyncio.ensure_future(check_if_warned())
    asyncio.ensure_future(claim_and_open())
    asyncio.ensure_future(hunt_and_fish())
    asyncio.ensure_future(pets())
    asyncio.ensure_future(main())
    loop.run_forever()
except KeyboardInterrupt:
    pass
finally:
    print("Closing Loop")
    loop.close()



